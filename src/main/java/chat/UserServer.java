package chat;

import chat.service.Server;
import chat.service.SocketAttributes;

public class UserServer {
    public static void main(String[] args) {

        SocketAttributes socketAttributes = new SocketAttributes();
        String host = socketAttributes.getHost();
        int port = socketAttributes.getPort();

        Server server = new Server();
        server.run(socketAttributes.getPort());

    }


}
