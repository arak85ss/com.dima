package chat.service;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public void run(String host, int port) {

        System.out.println("CLIENT CONNECTED");

        try (Socket socket = new Socket(host, port)) {

            Scanner scanner = new Scanner(socket.getInputStream());
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream());


            while (true) {

                Scanner sc = new Scanner(System.in);
                String line = sc.nextLine();
                printWriter.println(line);
                printWriter.flush();
                System.out.println(scanner.nextLine());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
