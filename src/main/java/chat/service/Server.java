package chat.service;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Server {


    public void run (int port) {
        System.out.print("SERVER: ");

        try(ServerSocket serverSocket = new ServerSocket(port)) {


           while(true) {
                Socket socket = serverSocket.accept();
                PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
                Scanner scanner = new Scanner(socket.getInputStream());
                System.out.println("CONNECTED");

                while(!socket.isClosed()) {
                    String line = scanner.nextLine();
                    System.out.println(line);
                    printWriter.println("RESP: ok");
                    printWriter.flush();

                }
            }

        }catch(Exception e) {
            e.printStackTrace();
        }
    }
}
