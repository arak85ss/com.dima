package chat;


import chat.service.*;
import org.w3c.dom.ls.LSOutput;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main  {

    public static void main(String[] args) {
        int choice;

        SocketAttributes socketAttributes = new SocketAttributes();
        String host = socketAttributes.getHost();
        int port = socketAttributes.getPort();


        Scanner scanner = new Scanner(System.in);
        System.out.println("Выберите вы сервер -[1], или клиент -[2]");
        choice = scanner.nextInt();


        //Server
        if(choice == 1) {
        System.out.println("Введите порт:");
        int portS = scanner.nextInt();
        socketAttributes.setPort(portS);

        Server server = new Server();
        System.out.println("Запустите программу: UserClient.java (в отдельной вкладке)");
        server.run(socketAttributes.getPort());
        System.out.println("Вы сервер: (ожидайте сообщение)");
        scanner.close();
        }


        //Client
            if(choice == 2) {
                System.out.println("Введите хост для подключения: ");
                String hostS  = scanner.next();
                socketAttributes.setHost(hostS);

                System.out.println("Введите порт для подключения: ");
                int portS = scanner.nextInt();
                socketAttributes.setPort(portS);

                System.out.println("Запустите программу: UserServer.java (в отдельной вкладке) и введите -[1]");
                int runClient = scanner.nextInt();
                System.out.println("Вы клиент: (введите сообщение)");

                Client client = new Client();
                client.run(socketAttributes.getHost(), socketAttributes.getPort());

            } else {
                System.out.println("Неверный выбор (или соединение оборвано), программа остановлена");
            }
    }
}