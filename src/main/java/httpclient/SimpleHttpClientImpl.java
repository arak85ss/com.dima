package httpclient;

import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SimpleHttpClientImpl implements SimpleHttpClient {


    @Override
    public SimpleHttpResponse request(HttpMethod method, String url, String payload, Map<String, String> headers) {
        //TODO: ваша задача реализовать данный метод

        /*
        default SimpleHttpResponse get(String url, Map<String, String> headers) {
            return request(HttpMethod.GET, url, null, headers);
        }

        default SimpleHttpResponse post(String url, String payload, Map<String, String> headers) {
            return request(HttpMethod.POST, url, payload, headers);
        }

         */



        URL urlResp = null;
        try {
            urlResp = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        SimpleHttpResponse resp = null;
        Map map;


        // GET
        if (method == HttpMethod.GET) {

            int port = urlResp.getPort() <= 0 ? 80 : urlResp.getPort();
            try (Socket socket = new Socket(urlResp.getHost(), port)) {
                //System.out.println(socket.isClosed());
                PrintStream printStream = new PrintStream(socket.getOutputStream());
                printStream.printf("%s %s HTTP/1.0\r\n", method, urlResp.getPath());


                for (Map.Entry<String, String> header : headers.entrySet()) {
                    printStream.printf("%s: %s\r\n", header.getKey(), header.getValue());
                }

                if (payload != null) {
                    printStream.printf("Content-Length: %d\r\n", payload.getBytes(StandardCharsets.UTF_8));
                }
                printStream.print("\r\n");


                if (payload != null) {
                    printStream.write(payload.getBytes(StandardCharsets.UTF_8));
                }
                printStream.flush();


                // Читаем ответ...
                Scanner scanner = new Scanner(socket.getInputStream());
                //System.out.println("Scanner");
                while (scanner.hasNextLine()) {

                    System.out.println(scanner.nextLine());
                }


            } catch (IOException e) {
                e.printStackTrace();
            }


            System.out.println("Metod" + method);


        }


            map = new HashMap();

                map.put("x-test-header-1", "header-value-1");
                map.put("x-test-header-2", "header-value-2");


            resp = new SimpleHttpResponse(200, "OK", "hello world", map);


        return  resp;
    }
/*
        default SimpleHttpResponse get(String url, Map<String, String> headers) {
            return request(HttpMethod.GET, url, null, headers);
        }

        default SimpleHttpResponse post(String url, String payload, Map<String, String> headers) {
            return request(HttpMethod.POST, url, payload, headers);
        }
 */






/*

        System.out.println(method.getMethodName());

        //SimpleHttpResponse resp = new SimpleHttpResponse(200,"OK",null, Map.of("1", "x-test-header-1","2", "x-test-header-2"));
        Map map = new HashMap();
        map.put("x-test-header-1", "header-value-1");
        map.put("x-test-header-2", "header-value-2");
        // SimpleHttpResponse resp = new SimpleHttpResponse(200,"OK","hello world", map);

        SimpleHttpResponse resp = new SimpleHttpResponse(200, "OK", "hello world", map);

        return resp;
    }




/*
        @Test
        void shouldMakeGetRequest() {
            stubTest1();
            SimpleHttpClient client = new SimpleHttpClientImpl();
            SimpleHttpResponse resp = client.get("http://127.0.0.1:1234/get-request-1", Map.of("x-test-request-header","hv"));
            System.out.println(resp);
            assertEquals(200,resp.getStatusCode());
            assertEquals("OK",resp.getStatusText());
            assertEquals("hello world",resp.getPayload().trim());
            assertEquals("header-value-1",resp.getHeaders().get("x-test-header-1"));
            assertEquals("header-value-2",resp.getHeaders().get("x-test-header-2"));
        }

        @Test
        void shouldMakePostRequest() {
            stubTest2();
            SimpleHttpClient client = new SimpleHttpClientImpl();
            SimpleHttpResponse resp = client.post(
                    "http://127.0.0.1:1234/post-request-1",
                    "testbody" ,
                    Map.of("Content-Type","text/plain")
            );
            System.out.println(resp);
            assertEquals(200,resp.getStatusCode());
            assertEquals("OK",resp.getStatusText());
            assertEquals("hello world",resp.getPayload().trim());
        }




 */





}
