package httpclient;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum HttpMethod {
    GET("GET"),POST("POST"),PUT("PUT"),DELETE("DELETE");
    @Getter
    private final String methodName;
}
